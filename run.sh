#!/bin/bash

date "+%T"
cp $1 build/graph.graph
python3 src/part1/unified.py
java -classpath src/part2/GraphCompress/out/production/GraphCompress Main
python3 src/part3/encode.py
date "+%T"
