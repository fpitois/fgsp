This code is under CC BY-NC 4.0

Graphs provided in the directory `graph_batch` are taken from the following databases:
- Leskovec J. and Krevl A.: *SNAP Datasets: Stanford Large Network Dataset Collection* (2014), http://snap.stanford.edu/data
- Rossi, R.A. and Ahmed, N.K.: *The network data repository with interactive graph analytics and visualization*. In: Proceedings of the TwentyNinth AAAI Conference on Artificial Intelligence (2015). http://networkrepository.com
- Rozemberczki, B., Allen, C. and Sarkar, R.: *Multi-scale Attributed Node Embedding* (2019)
