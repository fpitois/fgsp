import sys, os

# Disable print
sys.stdout = open(os.devnull, 'w')

import networkx as nx
from cdlib import algorithms, readwrite
from json import loads
from math import sqrt

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import community as community_louvain
import metis

from slashburn import k_slashburn, slashburn

# Restore print
sys.stdout = sys.__stdout__

PATH = "build/"
GRAPH = PATH + "graph.graph"
OUT = PATH + "struct.txt"

def myprint(f,x):
    print(x)
    f.write(x + '\n')

def list_list_to_string(ll, spec="fc"):
    return '\n'.join(map(lambda l: spec + " " + ' '.join(map(str, l)), ll))

def map_list_list(f, ll):
    return [list(map(f,l)) for l in ll]

def prune_commu(ll, k):
    return [l for l in ll if len(l) >= k]

def partition_to_commu(p):
    n = 1+max(p)
    commu = [[] for _ in range(n)]
    for i, u in enumerate(p):
        commu[u].append(i)
    return commu

########################

def decorate_cdlib(algo):
    def f(G):
        return loads(algo(G).to_json())["communities"]
    return f

@decorate_cdlib
def big_clam(G):
    return algorithms.big_clam(G)

@decorate_cdlib
def louvain(G):
    return algorithms.louvain(G)

@decorate_cdlib
def kcut(G):
    return algorithms.kcut(G)

def sqrt_metis(G):
    k = int(sqrt(G.order()/2))
    _, parts = metis.part_graph(G, k)
    return partition_to_commu(parts)

def trivial(G):
    return [list(range(G.order()))]

def unified():
    with open(GRAPH, 'r') as f:
        lines = f.readlines()
    G = nx.parse_edgelist(lines, nodetype=int, delimiter=",", data=False)
    G = nx.convert_node_labels_to_integers(G, label_attribute="old")
    threshold = 1
    '''
    Each function in flist takes a Graph G created with networkx such that V = [0..n-1] for some n,
    and returns a list of communities. A community is a list of numbers in [0..n-1]
    '''
    flist = [
        (slashburn, [], "sl"),
        (big_clam, [], "bc"),
        (louvain, [], "lv"),
        (sqrt_metis, [], "mt"),
    ]
    print("Part 1")
    i = 0
    with open(OUT, 'w') as f:
        for algo,args,spec in flist:
            commu = algo(G, *args)
            pruned = prune_commu(commu, threshold)
            reverse = map_list_list(lambda x: G.nodes[x]["old"], pruned)
            formated = list_list_to_string(reverse, spec=spec)
            f.write(formated + '\n')
            i += 1
            print("> Algorithm %d/%d done" % (i, len(flist)))

def main():
    unified()

main()
