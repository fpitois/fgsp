import networkx as nx
from math import sqrt

def add(l,x, key=lambda x:x):
    m = min([key(x) for x in l])
    if key(x) <= m:
        return
    for i in range(len(l)):
        if key(l[i]) == m:
            l[i] = x
            return


def k_max(l,k, key=lambda x:x):
    if len(l) <= k:
        return l
    ans = []
    for x in l:
        if len(ans) < k:
            ans.append(x)
        else:
            add(ans,x,key)
    return ans

def k_struct(H, k, adaptative=False):
    G = H.copy()
    while G.order() > k:
        while G and nx.is_connected(G):
            sorted_degree = sorted(G.degree, key = lambda x: x[1], reverse=True)
            k_center = sorted_degree[:k]
            # k_center = k_max(G.degree, k, key = lambda x: x[1])
            center = []
            for v, d in k_center:
                center.append(v)
                G.remove_node(v)
            print(">>> removing center (%d nodes), %d remaining" % (k, G.order()))
            yield set(center)
        if G:
            sorted_comp = sorted(list(nx.connected_components(G)), key = len)
            bcc = sorted_comp[-1]
            spokes = set()
            for component in sorted_comp[:-1]:
                spokes = spokes.union(component)
            yield spokes

            # G.remove_nodes_from([n for n in G if n not in set(bcc)])
            to_remove = [n for n in G if n in spokes]
            G.remove_nodes_from(to_remove)
            print(">>> removing spokes (%d nodes), %d remaining" % (len(to_remove), G.order()))
            if adaptative:
                k = max(10,G.order()//1000)

def k_slashburn(*args, **kwargs):
    last = []
    for s in k_struct(*args, **kwargs):
        last.append(s)
    return last

def slashburn(G):
    return k_slashburn(G, max(10,G.order()//1000), adaptative=True)
