from math import ceil

PATH = "build/"
THRESHOLD = 8192

def listMap(f,x): return list(map(f,x))

def keySplit(s):
    pq = s.split()
    return (int(pq[0]), int(pq[1]))

def stringToStruct(string):
    string.replace('\n', '')
    struct = string[3:]
    vertexList = []
    for vertexString in struct.split(' '):
        if vertexString: vertexList.append(int(vertexString))
    return vertexList

def splitLongString(string, threshold):
    count = 0
    partIndex = [0]
    for i in range(len(string)):
        if string[i] == '1':
            count += 1
        if count > threshold:
            partIndex.append(i)
            count -= threshold
    partIndex.append(len(string))
    partList = []
    for i in range(len(partIndex)-1):
        start = partIndex[i]
        end = partIndex[i+1]
        partList.append(string[start:end])
    return partList

def getEdge(fileGraph):
    with open(fileGraph, 'r') as f:
        edgeList = f.readlines()
    def stringToCouple(x):
        [i,j,w] = x.replace('\n', '').split(',')
        ni = min(int(i), int(j))
        nj = max(int(i), int(j))
        return(int(ni),int(nj))
    edgeList = listMap(stringToCouple, edgeList)
    return(edgeList)

def getPartition(filePartition):
    with open(filePartition, 'r') as f:
        partList = f.readlines()
    partList = listMap(stringToStruct, partList)
    return(partList)

def writeBinary(binary_string, file):
    data = bytes(int(binary_string[i:i+8], 2) for i in range(0, len(binary_string), 8))
    with open(file, 'wb') as f:
        f.write(data)

############

def choose(n, k):
    """
    A fast way to calculate binomial coefficients by Andrew Dalke.
    See http://stackoverflow.com/questions/3025162/statistics-combinations-in-python
    """
    if 0 <= k <= n:
        ntok = 1
        ktok = 1
        for t in range(1, min(k, n - k) + 1):
            ntok *= n
            ktok *= t
            n -= 1
        if k % 100 == 0:
            print(">>>", n, k)
        return ntok // ktok
    else:
        return 0

def ar_encode(list):
    sum = 0
    k = 1
    for i, x in enumerate(list[::-1]):
        if x in [1, '1']:
            sum += choose(i,k)
            k += 1
    return sum

def encodeBigInt(n):
    a = bin(n)[2:]
    b = bin(len(a))[2:]
    n = ceil(len(b)/4)
    c = '1' * n + '0'
    return c + b.zfill(4*n) + a

def encodeSmallInt(n):
    a = bin(n)[2:]
    n = ceil(len(a)/4)
    b = '0' * n + '1'
    return b + a.zfill(4*n)

def encodeInt(n):
    if n <= 2**25:
        return encodeSmallInt(n)
    return encodeBigInt(n)

############

class GraphEncode():
    def __init__(self, edgeList, partList):
        self.edgeList = edgeList
        self.partList = partList
        self.order = sum(listMap(len,partList))
        self.vertexAssign = [] # if i = vertexAssign[vertex], then vertex is in partList[i]
        self.vertexRank = [] # if i = vertexAssign[vertex] and j = vertexRank[vertex], then partList[i][j] == vertex
        self.blocDict = {}

        self._computeVertexPOV()
        print("> Graph imported")
        for edge in self.edgeList:
            self._addEdge(edge)

    def _computeVertexPOV(self):
        n = self.order
        self.vertexAssign = [0]*n
        self.vertexRank = [0]*n
        for i,part in enumerate(self.partList):
            for j,vertex in enumerate(part):
                self.vertexAssign[vertex] = i
                self.vertexRank[vertex] = j

    def _addEdge(self, edge):
        part1 = self.vertexAssign[edge[0]]
        part2 = self.vertexAssign[edge[1]]
        if part1 > part2:
            part1, part2 = part2, part1
            edge = edge[::-1]
        key = str(part1) + " " + str(part2)
        if key in self.blocDict:
            self.blocDict[key].append(edge)
        else:
            self.blocDict[key] = [edge]

    def getPartSize(self, part):
        return len(self.partList[part])

    def getSortedBlocList(self):
        return sorted(map(lambda x: (keySplit(x[0]), x[1]), self.blocDict.items()))

    def metaMatrixToString(self):
        n = len(self.partList)
        print(">>> Data made of %d bits (%d ones)" % ((n*(n-1))//2, len(self.blocDict)))
        ans = [[0] * n for _ in range(n)]
        for key, value in self.blocDict.items():
            p,q = keySplit(key)
            ans[p][q] = 1
        s = ""
        for k,l in enumerate(ans):
            for b in l[k:]:
                s += str(b)
        return s

    def blockToString(self, i, j):
        assert i <= j
        key = str(i) + " " + str(j)
        if key not in self.blocDict:
            return ""
        p = self.getPartSize(i)
        q = self.getPartSize(j)
        print(">>> Data made of %d bits (%d ones)" % (p*q, len(self.blocDict[key])))
        ans = [[0] * q for _ in range(p)]
        for edge in self.blocDict[key]:
            x = self.vertexRank[edge[0]]
            y = self.vertexRank[edge[1]]
            ans[x][y] = 1
        s = ""
        if i == j:
            for k,l in enumerate(ans):
                for b in l[k+1:]:
                    s += str(b)
        else:
            for l in ans:
                for b in l:
                    s += str(b)
        return s

    def encodeBlock(self, i, j):
        s = ""
        for part in splitLongString(self.blockToString(i, j), THRESHOLD):
            s += encodeInt(ar_encode(part))
        return s

    def encodeMetaMatrix(self):
        s = ""
        for part in splitLongString(self.metaMatrixToString(), THRESHOLD):
            s += encodeInt(ar_encode(part))
        return s

    def encode(self):
        s = ""
        s += encodeInt(self.order)
        s += encodeInt(len(self.partList))
        for i in range(len(self.partList)):
            s += encodeInt(self.getPartSize(i))
        print("> Part lenghs encoded")
        s += encodeInt(len(self.blocDict))
        s += self.encodeMetaMatrix()
        print("> Meta-matrix encoded")
        l = self.getSortedBlocList()
        for kv in l:
            value = kv[1]
            s += encodeInt(len(value))
        print("> Header encoded, current size is %d bits" % len(s))
        for i, kv in enumerate(l):
            key = kv[0]
            s += self.encodeBlock(*key)
            print("> Bloc %d/%d encoded, current size is %d bits" % (i, len(l), len(s)))
        return s

############

def main():
    print("Part 3")
    fileGraph = PATH + "graph.graph"
    filePartition = PATH + "partition.txt"
    fileOut = PATH + "compress.out"
    edgeList = getEdge(fileGraph)
    partList = getPartition(filePartition)
    graph = GraphEncode(edgeList, partList)
    output = graph.encode()
    writeBinary(output, fileOut)
    print("> Encoding done in %d bits" % len(output))

main()
