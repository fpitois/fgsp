import static java.lang.Math.log;
import static java.lang.Math.min;
import static java.lang.Math.ceil;

public class CompressMath {
    private static final double log2 = log(2);

    public static int choose(int n, int k){
        if (0 <= k && k <= n){
            int nToken = 1;
            int kToken = 1;
            int bound = min(k, n-k);
            for (int t=1; t <= bound; t++){
                nToken *= n;
                kToken *= t;
                n -= 1;
            }
            return nToken / kToken;
        }
        return 0;
    }

    public static double entropy(double x){
        return -(1-x)*log(1-x)/log2 - x*log(x)/log2;
    }

    public static double entropy(int p, long q){
        double x = (double) p / (double) q;
        return -(1-x)*log(1-x)/log2 - x*log(x)/log2;
    }

    public static double intSize(double n){
        if (n < 1){
            return 1;
        }
        return ceil(log(n)/log2);
    }

    public static int arithmetic(int n,int m){
        // n = nbVertices
        // m = nbEdges
        double nCast = (double) n;
        double mCast = (double) m;
        double nSquare = (double) (n*n);
        double x = 2 * mCast / nSquare;
        int term1 = (int) (ceil(entropy(x) * nSquare / 2));
        int term2 = (int) (intSize(nCast));
        int term3 = (int) (2*intSize(intSize(nCast)));
        return term1 + term2 + term3;
    }

    public static int arithmeticSub(int ones, long total){
        double oneCast = (double) ones;
        int term1 = (int) (ceil(entropy(ones, total) * total));
        int term2 = (int) (intSize(oneCast));
        int term3 = (int) (2*intSize(intSize(oneCast)));
        return term1 + term2 + term3;
    }

    public static int arithmeticSubSum(Pic[] picList){
        int s = 0;
        for (Pic pic : picList){
            s += pic.arithmeticSub();
        }
        return  s;
    }
}
