import java.util.ArrayList;

public class Greedy {
    String path;
    Graph graph;
    ArrayList<Structure> structList;
    ArrayList<Integer> bestStructList;
    ArrayList<Score> score; // size of score should always be (size of structList - size of bestStructList)

    public Greedy(String path){
        this.path = path;
        String graphFilename = path + "graph.graph";
        String structFilename = path + "struct.txt";
        this.graph = Graph.fromFile(graphFilename);
        this.structList = Structure.listFromFile(structFilename);
        this.bestStructList= new ArrayList<Integer>();
        this.score = new ArrayList<Score>();
    }

    public void updateScore(){
        int i;
        int n = structList.size();
        Structure struct;
        int currentDiskSpace;

        this.score.clear();

        for (i = 0; i < n; i++) {
            if (!bestStructList.contains(i)) {
                struct = structList.get(i);
                graph.undoInit();
                graph.addStructure(struct);
                currentDiskSpace = graph.diskSpace();
                this.score.add(new Score(i, currentDiskSpace, struct));
                graph.undoAction();
            }
        }

        this.score.sort(Score.C);
    }

    public void addBestStructure(){
        int i = 0;
        int j = this.bestStructList.size();
        int n = this.score.size();
        Structure struct;
        int structIndex;
        int currentDiskSpace;
        int bestCompression = graph.diskSpace();
        while (i < n) {
            struct = score.get(i).getStruct();
            structIndex = score.get(i).getIndex();
            graph.undoInit();
            graph.addStructure(struct);
            currentDiskSpace = graph.diskSpace();
            if (currentDiskSpace < bestCompression) {
                bestCompression = currentDiskSpace;
                bestStructList.add(structIndex);
                graph.addStructure(struct);
                System.out.println("> Selecting structure " + structIndex + " as n°" + j + ", estimate size is " + graph.diskSpace() + " bits");
                j++;
                i++;
            } else {
                graph.undoAction();
                i = n; // break
            }
        }
    }

    public void run(){
        String partFilename = this.path + "partition.txt";

        System.out.println("Part 2");
        System.out.println("> Without structure, estimate size is " + graph.diskSpace() + " bits");

        int prev_size = -1;
        int new_size = 0;
        while(prev_size < new_size){
            prev_size = bestStructList.size();
            System.out.println("> Updating scores...");
            this.updateScore();
            this.addBestStructure();
            new_size = bestStructList.size();
        }

        FileIO.write(graph.getPartition().toFormattedString(), partFilename);
    }
}
