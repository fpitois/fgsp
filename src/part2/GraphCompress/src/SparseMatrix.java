import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class SparseMatrix<E extends Positional> implements Iterable<E> {
    private int size;
    private ArrayList<ArrayList<E>> matrix;
    private int undoSize;
    private ArrayList<E> undoList;

    public SparseMatrix() {
        this.size = 0;
        this.undoSize = -1;
        this.matrix = new ArrayList<ArrayList<E>>();
        this.undoList = new ArrayList<E>();
    }

    // Guaranty that the matrix has size at least n
    // No guaranty on the exact size
    public void scale(int n){
        this.size = n;
        while(this.matrix.size() < n)
            this.matrix.add(new ArrayList<E>(Collections.nCopies(1 + this.matrix.size(), null)));
    }

    public E get(int i, int j){
        return this.matrix.get(max(i,j)).get(min(i,j));
    }
    public boolean hasValue(int i, int j){return this.get(max(i,j), min(i,j)) != null;}

    public void set(int i, int j, E element){
        this.matrix.get(max(i,j)).set(min(i,j), element);
    }
    public void delete(int i, int j){
        this.undoList.add(this.get(i,j));
        this.set(i,j, null);
    }

    public int getSize() {
        return size;
    }

    public void undoInit(){
        this.undoSize = this.size;
        this.undoList = new ArrayList<E>();
    }

    public void undoAction(){
        // Undo actions in reverse order
        for(int i = this.undoList.size() - 1; i>= 0; i--){
            E element = this.undoList.get(i);
            this.set(element.getRow(), element.getColumn(), element);
        }
        for(int i = this.size; i > this.undoSize; i--)
            this.matrix.remove(i-1);
        this.size = this.undoSize;
        this.undoSize = -1;
    }


    public Iterator<E> iterator() {
        return new Iterator<E> () {

            private int i = 0;
            private int j = -1;

            public boolean hasNext() {
                while(true){
                    j++;
                    if (j>i) {
                        j = 0;
                        i++;
                        if (i >= SparseMatrix.this.matrix.size())
                            return false;
                    }
                    if (SparseMatrix.this.hasValue(i,j))
                        return true;
                }
            }

            public E next() {
                if (!SparseMatrix.this.hasValue(i,j))
                    System.out.println("Erreur impossible");
                return SparseMatrix.this.get(i,j);
            }
        };
    }
    @Override
    public String toString() {
        String answer = "";
        for(E element: this)
            answer += element.toString() + "\n";
        return answer;
    }
}
