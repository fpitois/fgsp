import java.util.ArrayList;
import java.util.Objects;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class Block implements Positional{
    private ArrayList<Edge> edgeList;
    private int row;
    private int column;
    private int rowSize;
    private int columnSize;

    private int diskSpace;

    public Block(ArrayList<Edge> edgeList, int row, int column, int rowSize, int columnSize) {
        this.edgeList = edgeList;
        this.row = max(row, column);
        this.column =  min(row, column);
        this.rowSize = rowSize;
        this.columnSize = columnSize;
        this.diskSpace = -1 ;
    }

    public boolean isDiagonal() {
        return (this.row == this.column);
    }

    public boolean isEmpty() {
        return (this.edgeList.size() == 0);
    }

    public void addEdge(Edge edge){
        //if (!this.edgeList.contains((edge)))
            this.edgeList.add(edge);
        this.diskSpace = -1 ;
    }

    public ArrayList<Edge> getEdgeList() {
        return edgeList;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public int getDiskSpace(){
        if (this.diskSpace == -1){
            if (isDiagonal())
                this.diskSpace = CompressMath.arithmeticSub(this.edgeList.size(), (long) this.rowSize * (long) this.columnSize / 2);
            else
                this.diskSpace = CompressMath.arithmeticSub(this.edgeList.size(), (long) this.rowSize * (long) this.columnSize);
        }
        return this.diskSpace;
    }

    public Block clone(){
        return new Block(new ArrayList<Edge>(edgeList), row, column, rowSize, columnSize);
    }

    @Override
    public String toString() {
        String answer = "row: " + row + ", col: " + column + "; ";
        answer += edgeList.size() + "/(" + rowSize + "*" + columnSize + ")";
        /*
        for (Edge edge : edgeList)
            answer += edge.toString();
         */
        return  answer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Block block = (Block) o;
        return row == block.row && column == block.column && rowSize == block.rowSize && columnSize == block.columnSize && edgeList.equals(block.edgeList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(edgeList, row, column, rowSize, columnSize);
    }
}
