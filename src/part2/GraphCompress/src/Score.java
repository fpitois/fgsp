import java.util.Comparator;

public class Score {
    private int index;
    private int score;
    private Structure struct;
    public static final Comparator<? super Score> C = new Comparator<Score>(){
        @Override
        public int compare(Score lhs, Score rhs){
            return lhs.score > rhs.score ? 1 : (lhs.score < rhs.score ? -1 : 0);
        }
    } ;

    public Score(int index, int score, Structure struct){
        this.index = index;
        this.score = score;
        this.struct = struct;
    }

    public Structure getStruct(){return struct;}
    public int getIndex(){return index;}

    @Override
    public String toString() {
        return "(" + this.index + ", " + this.score + ")";
    }
}
