import java.util.ArrayList;

public class Main {
    public static void test1() {
        int n = 10;
        String edgeString = "0,1;0,2;0,3;1,2;1,3;2,3;2,4;2,5;3,5;4,6";
        Graph graph = new Graph(Edge.parseString(edgeString), n);
        Structure s1 = new Structure("fc 0 1 2 3");
        Structure s2 = new Structure("fc 2 3 4 5");
        graph.addStructure(s1);
        System.out.println(graph.toString());
        Graph copyGraph = graph.clone();
        copyGraph.undoInit();
        copyGraph.addStructure(s2);
        System.out.println(graph.toString());
        System.out.println(copyGraph.toString());
        copyGraph.undoAction();
        System.out.println(graph.toString());
        System.out.println(copyGraph.toString());
    }

    public static void test2(){
        String graphName;
        graphName = "as-oregon";
        graphName = "choc";
        String path;
        path = "/home/pitois/Documents/these/thesis/prog/compress/";
        path = "C:/Users/Pit/Documents/Boulot/Thèse/travail/thesis/prog/compress/";
        path = "../compress/";
        String graphFilename = path + "graph/" + graphName + ".graph";
        String structFilename = path + "struct/" + graphName + ".txt";
        Graph graph = Graph.fromFile(graphFilename);
        ArrayList<Structure> structList = Structure.listFromFile(structFilename);
        int i, n;
        Structure struct;
        n = structList.size();
        for (i=0; i<n; i++){
            struct = structList.get(i);
            graph.addStructure(struct);
        }

        ArrayList<Edge> edgeList = Edge.readFile(graphFilename);
        Graph graph2 = Graph.fromFile(graphFilename);
        for (i=0; i<n; i++){
            struct = structList.get(i);
            graph2.undoInit();
            graph2.addStructure(struct);
            graph2.undoAction();
            graph2.addStructure(struct);
            System.out.println(i + ": " + graph2.diskSpace());
        }
        System.out.println(graph.check(edgeList));
        System.out.println(graph2.check(edgeList));

        int row, column;
        for (Block block: graph.getBlockMatrix()){
            row = block.getRow();
            column = block.getColumn();
            if(!graph2.getBlockMatrix().hasValue(row, column)){
                System.out.println("Missing: " + row + ", " + column);
            }
        }

        //System.out.println(i + ": " + graph.diskSpace());
        //System.out.println(i + ": " + graph2.diskSpace());
        //System.out.println(graph);
        //System.out.println(graph.getPartition());
        //System.out.println(graph2);
        //System.out.println(graph2.getPartition());

        /*
        Iterator<Block> iterator = graph.getBlockMatrix().iterator();
        Iterator<Block> iterator2 = graph2.getBlockMatrix().iterator();
        Block block;
        Block block2;

        for (; iterator.hasNext() && iterator2.hasNext();) {
            block = iterator.next();
            block2 = iterator2.next();
            System.out.println(block);
            System.out.println(block2);
            System.out.println(block.equals(block2));
        }

         */
    }

    public static void greedy(){
        String path;
        path = "build/";
        // path = "../../../build/";
        String graphFilename = path + "graph.graph";
        String structFilename = path + "struct.txt";
        String partFilename = path + "partition.txt";
        Graph graph = Graph.fromFile(graphFilename);
        ArrayList<Structure> structList = Structure.listFromFile(structFilename);
        int i, k, n, m;
        n = structList.size();
        m = n / 10;
        m = n;
        Structure struct;
        ArrayList<Integer> bestStructList= new ArrayList<Integer>();
        int bestCompression = 10*graph.diskSpace();
        int bestStructure;
        int currentDiskSpace;
        System.out.println("Part 2");
        System.out.println("> Without structure, estimate size is " + graph.diskSpace() + " bits");
        int j = 0;
        while (true) {
            bestStructure = -1;
            k = 0;
            for (i = 0; i < n; i++) {
                if (!bestStructList.contains(i)) {
                    struct = structList.get(i);
                    graph.undoInit();
                    graph.addStructure(struct);
                    currentDiskSpace = graph.diskSpace();
                    if (currentDiskSpace < bestCompression) {
                        bestCompression = currentDiskSpace;
                        bestStructure = i;
                        k = 0;
                        //System.out.println(i);
                    } else {
                        k++;
                        if (k > m){
                            i = n; // break
                        }
                    }
                    graph.undoAction();
                }
            }
            if(bestStructure >= 0){
                bestStructList.add(bestStructure);
                graph.addStructure(structList.get(bestStructure));
                System.out.println("> Selecting structure " + bestStructure + " as n°" + j + ", estimate size is " + graph.diskSpace() + " bits");
                //FileIO.write(graph.getPartition().toFormattedString(), path + "partition" + j + ".txt");
                j++;
            } else {
                break;
            }
        }

        //System.out.println(graph.check(edgeList));
        FileIO.write(graph.getPartition().toFormattedString(), partFilename);

    }

    public static void main(String[] args) {
        /* run the old algorithm without speed-up */
        // greedy();

        /* run the new algorithm with SORTING speed-up */
        Greedy g = new Greedy("build/");
        g.run();
    }
}