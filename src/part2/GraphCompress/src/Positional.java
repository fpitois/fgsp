public interface Positional {
    public int getRow();
    public int getColumn();
}
