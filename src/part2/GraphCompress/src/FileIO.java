import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileIO {
    public static void write(String data, String pathname) {
        File file = new File(pathname);
        FileWriter f = null;
        try {
            f = new FileWriter(file);
            f.write(data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                f.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
