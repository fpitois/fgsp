import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class Graph {
    private final int nbVertices;
    private Partition partition;
    private SparseMatrix<Block> blockMatrix;
    private Partition copyPartition;

    public Graph(int nbVertices){
        this.nbVertices = nbVertices;
        this.partition = null;
        this.blockMatrix = null;
        this.copyPartition = null;
    }
    public Graph(ArrayList<Edge> edgeList, int nbVertices){
        Block block = new Block(edgeList, 0, 0, nbVertices, nbVertices);
        Structure[] structList = new Structure[0];

        this.nbVertices = nbVertices;
        this.partition = new Partition(nbVertices, structList, this);
        this.blockMatrix = new SparseMatrix<Block>();
        this.copyPartition = null;
        this.blockMatrix.scale(1);
        this.blockMatrix.set(0,0,block);
    }

    public static Graph fromFile(String filename){
        ArrayList<Edge> edgeList = Edge.readFile(filename);
        int n = 0;
        for(Edge edge: edgeList){
            n = max(n, max(edge.getU(), edge.getV()));
        }
        return new Graph(edgeList, n+1);
    }

    /*
    The graph clones creates a deep copy of the partition and a shallow copy of the blockMatrix.
    Hence, methods like addStructure operates on the new partition and on the original blockMatrix.
    This is not a problem, as SparseMatrix<E> is given undo operations to undo those method calls.
     */
    public Graph clone(){
        Graph graph = new Graph(this.nbVertices);
        graph.partition = this.partition.clone(graph);
        graph.blockMatrix = this.blockMatrix;
        graph.copyPartition = null;
        return graph;
    }

    public void addStructure(Structure structure){
        this.partition.addStructure(structure);
    }

    @Override
    public String toString() {
        String answer = this.partition.toString() + "\n";
        String matrix = this.blockMatrix.toString();
        return answer + matrix;
    }

    private void deleteBlock(int row, int column){
        this.blockMatrix.delete(row, column);
    }

    private void addEdge(Edge edge){
        int u = edge.getU();
        int v = edge.getV();
        int row = this.partition.getVertexAssignment(u);
        int column = this.partition.getVertexAssignment(v);
        if(this.blockMatrix.hasValue(row, column)){
            this.blockMatrix.get(row, column).addEdge(edge);
        } else {
            ArrayList<Edge> edgeList = new ArrayList<Edge>();
            edgeList.add(edge);
            Block block = new Block(edgeList, row, column, partition.getPartSize(u), partition.getPartSize(v));
            this.blockMatrix.set(row, column, block);
        }
    }

    public void split(int oldRowIndex, int targetSize) {
        int n = this.blockMatrix.getSize();
        int columnIndex;
        this.blockMatrix.scale(targetSize);
        for (columnIndex = 0; columnIndex < n; columnIndex++) {
            if (blockMatrix.hasValue(oldRowIndex, columnIndex)) {
                Block block = blockMatrix.get(oldRowIndex, columnIndex);
                this.deleteBlock(oldRowIndex, columnIndex);
                for (Edge edge : block.getEdgeList()) {
                    this.addEdge(edge);
                }
            }
        }
    }

    public int diskSpace(){
        int answer = 0;
        int nbBlock = 0;
        for (Block block : this.blockMatrix) {
            answer += block.getDiskSpace();
            nbBlock++;
        }
        answer += CompressMath.arithmetic(this.blockMatrix.getSize(), nbBlock);
        return answer;
    }

    public void undoInit(){
        this.blockMatrix.undoInit();
        this.copyPartition = this.partition.clone(this);
    }

    public void undoAction(){
        this.blockMatrix.undoAction();
        this.partition = this.copyPartition;
        this.copyPartition = null;
    }

    public boolean check(ArrayList<Edge> edgeList){
        int i = 0;
        int x, y;
        for(Block block: this.blockMatrix)
            for(Edge edge: block.getEdgeList()) {
                i++;
                x = this.partition.getVertexAssignment(edge.getU());
                y = this.partition.getVertexAssignment(edge.getV());
                if (block.getRow() != max(x,y)) {return false;}
                if (block.getColumn() != min(x,y)) {return false;}
                if(!edgeList.contains(edge)) {return false;}
            }
        return (i == edgeList.size());
    }

    public Partition getPartition() {
        return partition;
    }

    public SparseMatrix<Block> getBlockMatrix() {
        return blockMatrix;
    }
}
