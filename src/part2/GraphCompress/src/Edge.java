import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class Edge {
    private final int u;
    private final int v;

    public Edge(int u, int v) {
        this.u = max(u,v);
        this.v = min(u,v);
    }

    public int getU() {
        return u;
    }

    public int getV() {
        return v;
    }

    public static ArrayList<Edge> parseString(String edgeString){
        ArrayList<Edge> edgeList = new ArrayList<Edge>();
        String[] separated = edgeString.split(";");
        for (String edge : separated){
            String[] cut = edge.split(",");
            int u = Integer.parseInt(cut[0]);
            int v = Integer.parseInt(cut[1]);
            edgeList.add(new Edge(u,v));
        }
        return edgeList;
    }

    public static ArrayList<Edge> readFile(String filename){
        ArrayList<Edge> edgeList = new ArrayList<Edge>();
        try {
            File myObj = new File(filename);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                if (!data.isEmpty()) {
                    String[] cut = data.split(",");
                    int u = Integer.parseInt(cut[0]);
                    int v = Integer.parseInt(cut[1]);
                    edgeList.add(new Edge(u, v));
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return edgeList;
    }

    @Override
    public String toString() {
        return "(" + u + "," + v + ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edge edge = (Edge) o;
        return u == edge.u && v == edge.v;
    }

    @Override
    public int hashCode() {
        return Objects.hash(u, v);
    }
}
