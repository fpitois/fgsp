import java.util.ArrayList;
import java.util.Arrays;

public class Partition {
    private int nbVertices;
    private int[] vertexAssignmentList;
    private ArrayList<Structure> partList; //assert: partList is a partition of [nbVertices] into Structures
    private Graph graph;

    /*
    k in partList.get(i).getList() <=> vertexAssignmentList[k] == i
     */


    public Partition(){
        this.nbVertices = 0;
        this.vertexAssignmentList = null;
        this.partList = null;
        this.graph = null;
    }
    public Partition(int nbVertices, Structure[] structureList, Graph graph) {
        this.graph = graph;
        this.nbVertices = nbVertices;
        this.partList = new ArrayList<Structure>();
        this.vertexAssignmentList = new int[nbVertices];

        this.partList.add(new Structure(nbVertices));
        for(Structure structure: structureList){
            this.addStructure(structure);
        }
    }

    public Partition clone(Graph graph){
        int n = this.vertexAssignmentList.length;
        Partition partition = new Partition();
        partition.nbVertices = this.nbVertices;
        partition.vertexAssignmentList = new int[n];
        partition.partList = new ArrayList<Structure>();
        partition.graph = graph;
        System.arraycopy(this.vertexAssignmentList, 0, partition.vertexAssignmentList, 0, n);
        for(Structure structure: this.partList)
            partition.partList.add(structure.clone());
        return partition;
    }

    public void addStructure(Structure structure){
        int n = this.partList.size();
        int i, m;
        Structure part;
        ArrayList<Integer> todo = new ArrayList<Integer>();
        for(i=0; i<n; i++){
            part = this.partList.get(i);
            Structure intersection = new Structure(0);
            Structure difference = new Structure(0);
            part.split(structure, intersection, difference);
            if (!intersection.isEmpty()){
                this.partList.set(i, intersection);
                if (!difference.isEmpty()){
                    todo.add(i);
                    this.partList.add(difference);
                }
            }
        }
        m = this.partList.size();
        for(i=n; i<m; i++){
            part = this.partList.get(i);
            for(int vertex: part.getList()){
                this.vertexAssignmentList[vertex] = i;
            }
        }
        for(int j=0; j<todo.size(); j++){
            this.graph.split(todo.get(j),m);
        }
    }

    @Override
    public String toString() {
        return Arrays.toString(vertexAssignmentList) + "\n" + partList.toString();
    }

    public String toFormattedString() {
        String ans = "";
        int n = this.partList.size();
        int i;
        Structure part;
        for(i=0; i<n; i++) {
            part = this.partList.get(i);
            ans += part.toFormattedString() + "\n";
        }
        return ans;
    }

    public int getVertexAssignment(int i) { return vertexAssignmentList[i]; }
    public int getPartSize(int i) { return partList.get(vertexAssignmentList[i]).size(); }

    public ArrayList<Structure> getPartList() {
        return partList;
    }
}
