public class Pic {
    private final int nbOne;
    private final int nbPixel;

    public Pic(int nbOne, int nbPixel) {
        this.nbOne = nbOne;
        this.nbPixel = nbPixel;
    }

    public int getNbOne() {
        return nbOne;
    }

    public int getNbPixel() {
        return nbPixel;
    }

    public int arithmeticSub(){
        return CompressMath.arithmeticSub(this.getNbOne(), this.getNbPixel());
    }
}
