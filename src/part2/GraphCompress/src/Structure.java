import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class Structure {
    private int[] list;

    public Structure(int[] list) {
        this.list = list;
    }

    public Structure(int n) {
        this.list = new int[n];
        for (int i=0; i<n; i++)
            this.list[i]=i;
    }

    public Structure(ArrayList<Integer> list) {
        int n = list.size();
        this.list = new int[n];
        for(int i=0; i<n; i++){
            this.list[i] = list.get(i);
        }
    }

    public Structure(String input){
        String inputNoNewLine = input.replace("\n", "");
        String inputNoFC = inputNoNewLine.substring(3);
        String[] inputSplit = inputNoFC.split(" ");
        int n = inputSplit.length;
        this.list = new int[n];
        int i = 0;
        for (String vertex: inputSplit) {
            this.list[i] = parseInt(vertex);
            i++;
        }
    }

    public int[] getList() {
        return list;
    }
    public int size() {return list.length;}

    public void setListFromArray(ArrayList<Integer> list) {
        int n = list.size();
        this.list = new int[n];
        for (int i = 0; i < n; i++) {
            this.list[i] = list.get(i);
        }
    }

    public boolean contains(int vertex){
        for (int u: this.getList()){
            if (u == vertex)
                return true;
        }
        return false;
    }

    public void split(Structure structure, Structure sIntersection, Structure sDifference){
        ArrayList<Integer> intersection = new ArrayList(); // this inter structure
        ArrayList<Integer> difference = new ArrayList();  // this minus structure
        for (int vertex: this.getList()){
            if (structure.contains(vertex)){
                intersection.add(vertex);
            } else {
                difference.add(vertex);
            }
        }
        sIntersection.setListFromArray(intersection);
        sDifference.setListFromArray(difference);
    }

    public boolean isEmpty(){
        return this.list.length == 0;
    }

    public static ArrayList<Structure> listFromFile(String filename){
        ArrayList<Structure> structList = new ArrayList<Structure>();
        try {
            File myObj = new File(filename);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                if (!data.isEmpty()) {
                    structList.add(new Structure(data));
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return structList;
    }

    @Override
    public String toString() {
        return "Structure{" + Arrays.toString(list) + '}';
    }

    public String toFormattedString(){
        String ans = "pt";
        for (int k : this.list){
            ans += " " + Integer.toString(k);
        }
        return ans;
    }

    public Structure clone(){
        int n = this.size();
        int[] list = new int[n];
        System.arraycopy(this.list, 0, list, 0, n);
        return new Structure(list);
    }

}
