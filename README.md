# Fine-Grained Structural Partitioning

This is an algorithm for graph partitionning.
Run `run.sh path_to_graph`, where graph is a file containing one edge per line.
An edge should be written as `a,b,w`, where `a`, `b` and `w` are three integers :
- `a` is the ID of the first vertex,
- `b` is the ID of the second vertex,
- `w` is the weight of the edge.
Weights are ignored in this algorithm, but should be there for compatibily reasons.
