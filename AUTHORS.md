# Authors

1. François Pitois, IdHAL: francois-pitois. Affiliation : 1,2

# Affiliations

1. LIRIS, Villeurbanne, France (UMR 5205)
2. LIB, Dijon, France (EA 7534)
